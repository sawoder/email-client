const fs = require('fs');
const path = require('path');
const naughtyStrings = require('big-list-of-naughty-strings');

const pathToEmailsArchive = path.resolve(__dirname, '..', 'src', 'assets', 'emails.json');

const emailsSource = fs.readFileSync(pathToEmailsArchive, 'utf8');
/**
 * @type {{}[]}
 */
const emailsParsed = JSON.parse(emailsSource);

emailsParsed.unshift({
	from: 'ostapenko.me@gmail.com',
	to: ['andrew.logvinov@behavox.com'],
	cc: [],
	bcc: ['adventure@behavox.com'],
	subject: 'XSS test with big-list-of-naughty-strings <img src="404" onerror="alert(1)" />',
	body: naughtyStrings.join('\n\n'),
	date: new Date().toISOString(),
});

fs.writeFileSync(pathToEmailsArchive, JSON.stringify(emailsParsed, null, '  '), 'utf8');

module.exports = {
	env: {
		browser: true,
		es6: true,
		node: true,
	},
	extends: [
		'eslint:recommended',
		'plugin:@typescript-eslint/recommended',
		'plugin:@typescript-eslint/recommended-requiring-type-checking',
		'plugin:@angular-eslint/recommended',
		'plugin:import/errors',
		'plugin:import/warnings',
		'plugin:import/typescript',
		'plugin:optimize-regex/recommended',
		'plugin:promise/recommended',
		'plugin:sonarjs/recommended',
		'plugin:jsdoc/recommended',
		'prettier',
		'prettier/@typescript-eslint',
	],
	parser: '@typescript-eslint/parser',
	parserOptions: {
		project: ['tsconfig.json', 'tsconfig.app.json'],
		ecmaVersion: 2020,
		sourceType: 'module',
	},
	plugins: [
		'@angular-eslint/template',
		'@typescript-eslint',
		'import',
		'jsdoc',
		'prefer-arrow',
		'simple-import-sort',
		'promise',
		'optimize-regex',
		'sonarjs',
		'prettier',
	],
	rules: {
		'import/no-unresolved': ['error', { ignore: ['!'] }],
		'@typescript-eslint/no-empty-function': ['error', { allow: ['methods', 'constructors'] }],
		'prefer-arrow/prefer-arrow-functions': [
			'warn',
			{
				allowStandaloneDeclarations: true,
			},
		],
		'sort-imports': 'off',
		'simple-import-sort/sort': [
			'error',
			{
				groups: [['^\\u0000'], ['^@angular', '^rxjs'], ['^[^.!]'], ['^\\.'], ['!']],
			},
		],
	},
	settings: {
		'import/ignore': ['!'],
	},
};

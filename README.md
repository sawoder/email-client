# email-client
> Tool for working with an archive of emails

When performing this test task, I was inspired by a project that I had been developing in the previous 4 years: Mail.ru Mail - https://e.mail.ru/inbox

Therefore, the look of my project is a bit like Mail.ru Mail.

## Main features

- Letters viewing with pagination, search and filtering;
- Highlighting of the search results;
- Switchable messages grouping to threads;
- Dark mode theme;
- Compact view;
- Citation and Forwarding parsing;
- Saving application state in page address (you can safely refresh the page);
- Saving application settings to Web Storage;
- No vulnerabilities like XSS and memory leaks.

### Infrastructure features

- This project generated with [Angular CLI](https://github.com/angular/angular-cli);
- This project uses [Jest](https://jestjs.io/docs/en/getting-started) for unit testing (Test coverage is not complete. I added a few tests to demonstrate my skills);
- This project has a small UI-kit. For easy development I've integrated [Storybook](https://storybook.js.org/) into the project;
- This project uses `eslint` and `prettier` for code, `stylelint` for styles.

### Features Roadmap

You can find the log of my work on the test task in `TODO.md`. It is written in Russian.

It has a list of completed microtasks that I set myself in the process of working on a task.

## Production deployment

The project's deploy is setting up using the [Vercel for Bitbucket](https://vercel.com/bitbucket).

Production deployment: https://email-client-behavox.now.sh/threads

## Project structure

- `src/`
    - `main.ts` - main entry point
    - `styles.css` - base application styles
    - `variables.css` - css custom properties for UI themes
    - `assets/`
        - `emails.json` - list of emails to work with
    - `app/`
        - `core/`
            - `services/` - all project services
        - `shared/` - shared resources, like typings, utilities functions, common components, etc
        - `ui-kit/` - simple UI-kit
        - `entry-points/` - application sections entry points
            - `settings/` - settings section
            - `threads/` - letters section

## Angular CLI

This project generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.7.

### Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Storybook

Run `npm run storybook` to launch storybook for easy components development

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

Run `NAME=name npm run add-ui-block` to generate new component `name` in ui-kit folder with default settings.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `npm test` to execute the unit tests via [Jest](https://jestjs.io/).

import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AngularSvgIconModule } from 'angular-svg-icon';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SettingsModule } from './entry-points/settings/settings.module';
import { ThreadsModule } from './entry-points/threads/threads.module';
import { SharedModule } from './shared/shared.module';
import { UiKitModule } from './ui-kit/ui-kit.module';

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		CoreModule,
		HttpClientModule,
		UiKitModule,
		SharedModule,
		ThreadsModule,
		SettingsModule,
		AppRoutingModule,
		AngularSvgIconModule.forRoot(),
		BrowserAnimationsModule,
	],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {}

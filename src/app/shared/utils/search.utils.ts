import { MINIMUM_POSSIBLE_WORD_LENGTH_IN_SEARCH } from '../constants';
import { IContentWithOccurrences, IOccurrences } from '../types';
import { removeDuplicates } from './arrays.utils';

export const parseWords = (query: string, minimumPossibleWordLength: number): string[] => {
	return removeDuplicates(
		query
			.split(/\s+/i)
			.filter(word => word.length >= minimumPossibleWordLength)
			.map(word => word.toLowerCase()),
	).sort();
};

export const findOccurrences = (query: string, content: string): IOccurrences => {
	const words = parseWords(query, MINIMUM_POSSIBLE_WORD_LENGTH_IN_SEARCH);
	const occurrences: IOccurrences = [];
	for (const word of words) {
		const regexp = new RegExp(word, 'ig');
		for (const regExpMatchArray of content.matchAll(regexp)) {
			if (typeof regExpMatchArray.index === 'number') {
				occurrences.push([
					regExpMatchArray.index,
					regExpMatchArray.index + regExpMatchArray[0].length,
				]);
			}
		}
	}

	return occurrences
		.sort((left, right) => {
			return left[0] - right[0];
		})
		.reduce((occurrences, occurrence) => {
			if (occurrences.length === 0) {
				occurrences.push(occurrence);
			} else {
				const lastOccurrence = occurrences[occurrences.length - 1];
				if (lastOccurrence[1] > occurrence[0]) {
					lastOccurrence[1] = Math.max(lastOccurrence[1], occurrence[1]);
				} else {
					occurrences.push(occurrence);
				}
			}
			return occurrences;
		}, [] as IOccurrences);
};

export const addOccurrences = <T>(
	query: string,
	withOccurrences: IContentWithOccurrences<T>,
): IContentWithOccurrences<T> => {
	const occurrences = findOccurrences(query, withOccurrences.content);

	if (occurrences.length) {
		return {
			...withOccurrences,
			occurrences,
		};
	}

	return withOccurrences;
};

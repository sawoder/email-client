import { findOccurrences, parseWords } from './search.utils';

describe('Search Utilities - parseWords', () => {
	it('with simple request', () => {
		expect(parseWords('This is search query. Search query is used to search', 3)).toEqual([
			'query',
			'query.',
			'search',
			'this',
			'used',
		]);
	});

	it('with empty request', () => {
		expect(parseWords('', 3)).toEqual([]);
	});

	it('with short word query', () => {
		expect(parseWords('a b c de fg', 3)).toEqual([]);
	});

	it('with custom minimumPossibleWordLength', () => {
		expect(parseWords('This is search query. Search query is used to search', 6)).toEqual([
			'query.',
			'search',
		]);
	});
});

describe('Search Utilities - findOccurrences', () => {
	it('with one word', () => {
		expect(findOccurrences('word', 'words')).toEqual([[0, 4]]);
	});

	it('with some words', () => {
		expect(findOccurrences('word', 'password, words, word')).toEqual([
			[4, 8],
			[10, 14],
			[17, 21],
		]);
	});

	it('is case insensitive', () => {
		expect(findOccurrences('word', 'PASSWORD, WORDS, WORD')).toEqual([
			[4, 8],
			[10, 14],
			[17, 21],
		]);
	});

	it('works with long query', () => {
		expect(findOccurrences('Please, find these words', 'Please, find these words.')).toEqual([
			[0, 7],
			[8, 12],
			[13, 18],
			[19, 24],
		]);
	});

	it('combines intersecting intervals', () => {
		expect(findOccurrences('pine apple pineapple', 'pineapple')).toEqual([[0, 9]]);
	});

	it('does not combine adjacent intervals', () => {
		expect(findOccurrences('pine apple', 'pineapple')).toEqual([
			[0, 4],
			[4, 9],
		]);
	});

	it('returns an empty array if nothing is found', () => {
		expect(findOccurrences('nothing found', 'Please, does not find these words.')).toEqual([]);
	});

	it('returns an empty array if search query is empty', () => {
		expect(findOccurrences('', 'Please, does not find these words.')).toEqual([]);
	});
});

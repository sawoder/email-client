export type IOccurrences = Array<[number, number]>;

export type IContentWithOccurrences<T = {}> = T & {
	content: string;
	occurrences?: IOccurrences;
};

export interface IRawEmail {
	from: string;
	to: string[];
	cc: string[];
	bcc: string[];
	subject: string;
	body: string;
	date: string;
}

export enum EmailNodeType {
	paragraph = 'paragraph',
	line = 'line',
	linebreak = 'linebreak',
}

export enum SubjectActionType {
	forward = 'forward',
	reply = 'reply',
}

export enum ThreadType {
	simple = '0',
	grouped = '1',
}

export interface IEmailSubject {
	raw: string;
	normalized: IContentWithOccurrences;
	isEmpty: boolean;
	actions: SubjectActionType[];
}

export interface IEmailBody {
	raw: IContentWithOccurrences;
	message: IContentWithOccurrences;

	citation?: IContentWithOccurrences;
	forwarding?: IContentWithOccurrences<{ caption: string }>;
}
export interface IMessageSnippet {
	snippet: IContentWithOccurrences;
}

export interface Contact {
	address: string;
	avatarUrl: string;
}

export interface IMessage {
	_id: string;

	from: Contact;
	to: Contact[];
	cc: Contact[];
	bcc: Contact[];

	date: Date;

	subject: IEmailSubject;
	body: IEmailBody;
	snippet: IMessageSnippet;
}

export interface IThread {
	_id: string;
	date: Date;
	from: Contact;
	subject: IEmailSubject;
	messageSnippet: IMessageSnippet;
	messages: string[];
}

export interface IPage<Entity> {
	page: number;
	pagesCount: number;
	allEntitiesCount: number;

	hasPrevious: boolean;
	hasNext: boolean;
	outOfRange: boolean;
	isEmpty: boolean;

	entities: Entity[];
}

export interface ISettings {
	pageSize: 15 | 25 | 50;
	darkModeEnabled: boolean;
	compactViewEnabled: boolean;
	messagesGroupingEnabled: boolean;
}

export interface IFullThread {
	thread: IThread;
	messages: IMessage[];
}

export interface ISearchQuery {
	qQuery?: string;
	qFrom?: string;
	qTo?: string;
	qDateFrom?: Date | null;
	qDateTo?: Date | null;
}

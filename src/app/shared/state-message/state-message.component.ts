import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

import chromeReaderMode from '!!raw-loader!material-design-icons/action/svg/production/ic_chrome_reader_mode_24px.svg';
import findInPage from '!!raw-loader!material-design-icons/action/svg/production/ic_find_in_page_24px.svg';
import viewList from '!!raw-loader!material-design-icons/action/svg/production/ic_view_list_24px.svg';
import sentimentDissatisfied from '!!raw-loader!material-design-icons/social/svg/production/ic_sentiment_dissatisfied_24px.svg';

type StateMessageType =
	| 'not-found-threads'
	| 'loading'
	| 'initialization-failed'
	| 'not-selected'
	| 'not-found-thread';

@Component({
	selector: 'state-message',
	templateUrl: 'state-message.component.html',
	styleUrls: ['state-message.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StateMessageComponent implements OnInit {
	@Input() mode: StateMessageType = 'loading';

	initializationFailedIcon = sentimentDissatisfied;
	notFoundThreadIcon = chromeReaderMode;
	notSelectedIcon = viewList;
	notFoundThreadsIcon = findInPage;

	constructor() {}

	ngOnInit(): void {}
}

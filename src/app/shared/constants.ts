export const MINIMUM_POSSIBLE_WORD_LENGTH_IN_SEARCH = 3;
export const DARK_MODE_CLASSNAME = 'dark';
export const COMPACT_VIEW_CLASSNAME = 'compact';

export const SUBJECT_PREFIXES_RE = /^(re:\s?|fwd?:\s?)*/i;
export const SUBJECT_PREFIX_RE = /^re:$/i;
export const SUBJECT_PREFIX_FWD = /^fwd?:$/i;
export const BODY_CITATION_RE = /-{5}original message-{5}/i;
export const BODY_FORWARDING_RE = /-{22} (forwarded by .+?) -{27}/i;
export const SNIPPET_LENGTH = 256;

export const STORAGE_KEY = 'settings-storage-v1';

export const MAXIMUM_FIELD_LENGTH = 50;

export const FIRST_PAGE_NUMBER = 1;

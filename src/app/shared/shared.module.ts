import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { UiKitModule } from '../ui-kit/ui-kit.module';
import { StateMessageComponent } from './state-message/state-message.component';

@NgModule({
	declarations: [StateMessageComponent],
	imports: [BrowserModule, HttpClientModule, UiKitModule],
	providers: [],
	bootstrap: [],
	exports: [StateMessageComponent],
})
export class SharedModule {}

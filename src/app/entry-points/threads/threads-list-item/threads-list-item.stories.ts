import { ThreadsListItemComponent } from './threads-list-item.component';
import { moduleMetadata } from '@storybook/angular';
import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { RouterModule } from '@angular/router';
import { TextComponent } from '../../../ui-kit/text/text.component';
import { IconComponent } from '../../../ui-kit/icon/icon.component';
import { ButtonComponent } from '../../../ui-kit/button/button.component';
import { IThread, SubjectActionType } from '../../../shared/types';
import { AvatarComponent } from '../../../ui-kit/avatar/avatar.component';
import { HighlightComponent } from '../../../ui-kit/highlight/highlight.component';

export default {
	title: 'ThreadsListItem',
	component: ThreadsListItemComponent,
	decorators: [
		moduleMetadata({
			declarations: [
				TextComponent,
				ThreadsListItemComponent,
				ButtonComponent,
				IconComponent,
				AvatarComponent,
				HighlightComponent,
			],
			imports: [
				BrowserModule,
				CommonModule,
				HttpClientModule,
				RouterModule.forRoot([{ path: '**', component: TextComponent }]),
				AngularSvgIconModule.forRoot(),
			],
			providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
		}),
	],
};

const threads: IThread[] = [
	{
		_id: '0:1111',
		date: new Date('2020-06-01T12:00:00.000+0000'),
		from: {
			address: 'mail@gmail.com',
			avatarUrl: `https://robohash.org/mail@gmail.com?size=96x96&bgset=bg2`,
		},
		subject: {
			raw: 'Hello',
			normalized: { content: 'Hello' },
			actions: [],
			isEmpty: false,
		},
		messageSnippet: {
			snippet: { content: 'Hello, friend', occurrences: [[0, 5]] },
		},
		messages: ['1111'],
	},
	{
		_id: '0:3333',
		date: new Date('2020-02-10T12:00:00.000+0000'),
		from: {
			address: 'interview-address-no-reply@gmail.com',
			avatarUrl: `https://robohash.org/interview@gmail.com?size=96x96&bgset=bg2`,
		},
		subject: {
			raw: 'FWD: Final Internal Review for Sign Off: Topics and Related Objectives',
			normalized: {
				content: 'Final Internal Review for Sign Off: Topics and Related Objectives',
				occurrences: [[6, 14]],
			},

			actions: [SubjectActionType.forward],
			isEmpty: false,
		},
		messageSnippet: {
			snippet: {
				content:
					'Kirk, I would modify this document slightly.  The first section is titled "Introduction to Risk Management" and the second section is "Hedging Instruments/Risk Management Products".',
			},
		},
		messages: [
			'3333',
			'3333',
			'3333',
			'3333',
			'3333',
			'3333',
			'3333',
			'3333',
			'3333',
			'3333',
			'3333',
			'3333',
			'3333',
			'3333',
		],
	},
	{
		_id: '0:2222',
		date: new Date('2020-06-01T10:30:00.000+0000'),
		from: {
			address: 'company@gmail.com',
			avatarUrl: `https://robohash.org/company@gmail.com?size=96x96&bgset=bg2`,
		},
		subject: {
			raw: 'RE:',
			normalized: { content: '' },
			actions: [SubjectActionType.reply],
			isEmpty: true,
		},
		messageSnippet: {
			snippet: { content: 'Order Info in attachment' },
		},
		messages: ['2220', '2221', '2220'],
	},
];

export const ThreadsListItemBase = () => ({
	component: ThreadsListItemComponent,
	props: {
		threads,
	},
	template: `
		<div style="max-width: 400px;">
			<threads-list-item
				*ngFor="let thread of threads"
				[thread]="thread"
				[href]="'/thread/' + thread._id"
				[active]="thread._id === '0:3333'"
			></threads-list-item>
		</div>
	`,
});

ThreadsListItemBase.story = {
	name: 'base view',
};

import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

import { IThread } from '../../../shared/types';

@Component({
	selector: 'threads-list-item',
	templateUrl: 'threads-list-item.component.html',
	styleUrls: ['threads-list-item.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ThreadsListItemComponent implements OnInit {
	@Input() thread: IThread | undefined;
	@Input() href = '';
	@Input() active?: boolean | string;

	constructor() {}

	ngOnInit(): void {}
}

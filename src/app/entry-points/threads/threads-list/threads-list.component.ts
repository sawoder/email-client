import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, ReplaySubject } from 'rxjs';
import {
	distinctUntilChanged,
	filter,
	map,
	pairwise,
	startWith,
	switchMap,
	takeUntil,
	tap,
} from 'rxjs/operators';

import { EmailsStorageService } from '../../../core/services/emails-storage.service';
import {
	PageViewRouteBuilder,
	RouteBuilderService,
	ThreadViewRouteBuilder,
} from '../../../core/services/route-builder.service';
import { SearchQueryService } from '../../../core/services/search-query.service';
import { FIRST_PAGE_NUMBER } from '../../../shared/constants';
import { IPage, ISearchQuery, IThread } from '../../../shared/types';

@Component({
	selector: 'threads-list',
	templateUrl: 'threads-list.component.html',
	styleUrls: ['threads-list.component.css'],
})
export class ThreadsListComponent implements OnInit, OnDestroy {
	private destroy: ReplaySubject<null> = new ReplaySubject(1);
	loading = true;
	currentPage = 0;
	emailsPage: IPage<IThread> | null = null;

	selectedThreadId?: string | null;

	@ViewChild('threadsList') threadsList?: ElementRef<HTMLElement>;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private emailsStorageService: EmailsStorageService,
		private routeBuilderService: RouteBuilderService,
		private searchQueryService: SearchQueryService,
	) {}

	threadViewRouteBuilder: ThreadViewRouteBuilder = () => '';
	pageViewRouteBuilder: PageViewRouteBuilder = () => '';

	ngOnInit(): void {
		this.routeBuilderService.threadViewRouteBuilderObservable
			.pipe(takeUntil(this.destroy))
			.subscribe(
				threadViewRouteBuilder => (this.threadViewRouteBuilder = threadViewRouteBuilder),
			);
		this.routeBuilderService.pageViewRouteBuilderObservable
			.pipe(takeUntil(this.destroy))
			.subscribe(pageViewRouteBuilder => (this.pageViewRouteBuilder = pageViewRouteBuilder));

		this.route.paramMap
			.pipe(
				map(paramMap => paramMap.get('threadId')),
				distinctUntilChanged(),
				takeUntil(this.destroy),
			)
			.subscribe(threadId => (this.selectedThreadId = threadId));

		const currentPage$ = this.route.paramMap.pipe(
			map(paramMap => this.parsePage(paramMap.get('page'))),
			distinctUntilChanged(),
		);

		const searchQuery$ = this.searchQueryService.searchQueryObservable;

		combineLatest([currentPage$, searchQuery$])
			.pipe(
				startWith([0, {}] as [number, ISearchQuery]),
				pairwise(),
				map(([[lastPage, lastSearchQuery], [currentPage, searchQuery]]) => {
					return {
						currentPage,
						searchQuery,
						pageChanged: currentPage !== lastPage,
						searchQueryChanged: !this.searchQueryService.isQueriesSame(
							lastSearchQuery,
							searchQuery,
						),
					};
				}),
				filter(({ pageChanged, searchQueryChanged }) => pageChanged || searchQueryChanged),
				tap(({ currentPage, searchQueryChanged }) => {
					this.loading = true;
					this.currentPage = currentPage;
					if (searchQueryChanged) {
						this.emailsPage = null;
					}
					this.routeBuilderService.updateCurrentPage(currentPage);
				}),
				switchMap(({ currentPage }) => {
					return this.emailsStorageService.loadThreadsPageFromServer(currentPage - 1);
				}),
				tap(emailsPage => {
					if (emailsPage.outOfRange) {
						this.redirectToFirstPage();
					} else {
						this.loading = false;
						this.emailsPage = emailsPage;
						this.scrollListToTop();
					}
				}),
				takeUntil(this.destroy),
			)
			.subscribe();
	}

	ngOnDestroy(): void {
		this.destroy.next(null);
		this.destroy.complete();
	}

	private parsePage(page: string | undefined | null): number {
		if (!page) {
			return FIRST_PAGE_NUMBER;
		}
		const parsed = Number.parseInt(page, 10);
		if (!Number.isNaN(parsed) && parsed.toString(10) === page && parsed > 0) {
			return parsed;
		}

		return FIRST_PAGE_NUMBER;
	}

	private scrollListToTop(): void {
		this.threadsList?.nativeElement.scrollTo({ top: 0 });
	}

	private redirectToFirstPage(): void {
		this.router.navigate([this.pageViewRouteBuilder(1)], { queryParamsHandling: 'merge' });
	}

	trackThread(index: number, thread: IThread): string {
		return thread._id;
	}
}

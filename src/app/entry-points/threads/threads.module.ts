import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

import { SharedModule } from '../../shared/shared.module';
import { UiKitModule } from '../../ui-kit/ui-kit.module';
import { LetterViewerComponent } from './letter-viewer/letter-viewer.component';
import { SearchQueryDialogComponent } from './search-query-dialog/search-query-dialog.component';
import { ThreadViewerComponent } from './thread-viewer/thread-viewer.component';
import { ThreadsListItemComponent } from './threads-list-item/threads-list-item.component';
import { ThreadsListComponent } from './threads-list/threads-list.component';
import { ThreadsRoutingModule } from './threads-routing.module';
import { ThreadsComponent } from './threads.component';

@NgModule({
	imports: [
		ThreadsRoutingModule,
		UiKitModule,
		CommonModule,
		MatDialogModule,
		MatButtonModule,
		MatFormFieldModule,
		MatInputModule,
		MatSlideToggleModule,
		MatDatepickerModule,
		MatNativeDateModule,
		ReactiveFormsModule,
		SharedModule,
	],
	declarations: [
		ThreadsComponent,
		ThreadsListComponent,
		ThreadsListItemComponent,
		ThreadViewerComponent,
		LetterViewerComponent,
		SearchQueryDialogComponent,
	],
	exports: [ThreadsComponent],
})
export class ThreadsModule {}

import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ReplaySubject } from 'rxjs';
import { distinctUntilChanged, filter, map, takeUntil } from 'rxjs/operators';

import { MAXIMUM_FIELD_LENGTH } from '../../../shared/constants';
import { ISearchQuery } from '../../../shared/types';
import { convertDateToUTC, isDate, isDatesSame, parseDate } from '../../../shared/utils/date.utils';

import clear from '!!raw-loader!material-design-icons/content/svg/production/ic_clear_24px.svg';
import arrowDown from '!!raw-loader!material-design-icons/hardware/svg/production/ic_keyboard_arrow_down_24px.svg';

interface IInternalFormValues extends ISearchQuery {
	dateRangeEnabled: boolean;
}

@Component({
	selector: 'ui-search-query-dialog',
	templateUrl: 'search-query-dialog.component.html',
	styleUrls: ['search-query-dialog.component.css'],
})
export class SearchQueryDialogComponent implements OnInit, OnDestroy {
	searchQueryForm: FormGroup;
	clearIcon = clear;
	arrowDownIcon = arrowDown;
	dateToMinimumValue?: Date | null;
	private destroy: ReplaySubject<null> = new ReplaySubject(1);

	constructor(
		private formBuilder: FormBuilder,
		public dialogRef: MatDialogRef<SearchQueryDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public searchQuery: ISearchQuery,
	) {
		const qDateTo = parseDate(searchQuery.qDateTo);
		const qDateFrom = parseDate(searchQuery.qDateFrom);
		this.searchQueryForm = this.formBuilder.group({
			qQuery: (searchQuery.qQuery || '').slice(0, MAXIMUM_FIELD_LENGTH),
			qTo: (searchQuery.qTo || '').slice(0, MAXIMUM_FIELD_LENGTH),
			qFrom: (searchQuery.qFrom || '').slice(0, MAXIMUM_FIELD_LENGTH),
			qDateFrom,
			qDateTo,
			dateRangeEnabled: !isDatesSame(qDateTo, qDateFrom),
		} as IInternalFormValues);
	}

	ngOnInit(): void {
		this.searchQueryForm.valueChanges
			.pipe(
				filter((values: IInternalFormValues) => values.dateRangeEnabled),
				filter(
					(values: IInternalFormValues) =>
						isDate(values.qDateFrom) &&
						isDate(values.qDateTo) &&
						values.qDateFrom.valueOf() > values.qDateTo.valueOf(),
				),
				map((values: IInternalFormValues) => values.qDateTo),
				distinctUntilChanged(),
				takeUntil(this.destroy),
			)
			.subscribe(() => {
				this.searchQueryForm.patchValue({ qDateTo: null });
			});

		this.searchQueryForm.valueChanges
			.pipe(
				filter((values: IInternalFormValues) => !values.dateRangeEnabled),
				map((values: IInternalFormValues) => values.qDateTo),
				distinctUntilChanged(),
				takeUntil(this.destroy),
			)
			.subscribe(() => {
				this.searchQueryForm.patchValue({ qDateTo: null });
			});

		this.searchQueryForm.valueChanges
			.pipe(
				map((values: IInternalFormValues) => values.qDateFrom),
				distinctUntilChanged(),
				takeUntil(this.destroy),
			)
			.subscribe(dateFrom => (this.dateToMinimumValue = dateFrom));
	}

	ngOnDestroy(): void {
		this.destroy.next(null);
		this.destroy.complete();
	}

	clear(key: keyof IInternalFormValues): void {
		this.searchQueryForm.patchValue({
			[key]: this.getEmptyValues()[key],
		});
	}

	clearAll(): void {
		this.searchQueryForm.setValue(this.getEmptyValues());
	}

	clearDate(event: MouseEvent, name: 'qDateFrom' | 'qDateTo'): void {
		event.stopPropagation();
		this.searchQueryForm.get(name)?.setValue(null);
	}

	onSubmit(): void {
		this.dialogRef.close(this.getClearForm());
	}

	getClearForm(): ISearchQuery {
		const query: ISearchQuery = {};
		const {
			qQuery,
			qFrom,
			qTo,
			qDateFrom,
			qDateTo,
			dateRangeEnabled,
		} = this.searchQueryForm.getRawValue() as IInternalFormValues;
		if (qQuery && qQuery.trim()) {
			query.qQuery = qQuery.trim();
		}
		if (qFrom && qFrom.trim()) {
			query.qFrom = qFrom.trim();
		}
		if (qTo && qTo.trim()) {
			query.qTo = qTo.trim();
		}
		if (qDateFrom) {
			const dateFrom = convertDateToUTC(qDateFrom);
			query.qDateFrom = dateFrom;
			if (!dateRangeEnabled) {
				query.qDateTo = dateFrom;
			}
		}
		if (qDateTo) {
			query.qDateTo = convertDateToUTC(qDateTo);
		}

		return query;
	}

	private getEmptyValues(): IInternalFormValues {
		return {
			qQuery: '',
			qTo: '',
			qFrom: '',
			qDateFrom: null,
			qDateTo: null,
			dateRangeEnabled: false,
		};
	}
}

import { Component, Input, OnInit } from '@angular/core';

import { Contact, IMessage } from '../../../shared/types';

import visibilityOn from '!!raw-loader!material-design-icons/action/svg/production/ic_visibility_24px.svg';
import visibilityOff from '!!raw-loader!material-design-icons/action/svg/production/ic_visibility_off_24px.svg';

@Component({
	selector: 'letter-viewer',
	templateUrl: 'letter-viewer.component.html',
	styleUrls: ['letter-viewer.component.css'],
})
export class LetterViewerComponent implements OnInit {
	@Input() message: IMessage | null = null;
	@Input() renderExpanded: boolean | string = false;
	expanded = false;
	messagePartVisible = false;

	visibilityOn = visibilityOn;
	visibilityOff = visibilityOff;

	constructor() {}

	private isRenderExpanded(): boolean {
		return !!(this.renderExpanded || this.renderExpanded === '');
	}

	ngOnInit(): void {
		if (this.isRenderExpanded()) {
			this.expanded = true;
		}
	}

	toggleLetter(): void {
		this.expanded = !this.expanded;
	}

	toggleMessagePart(): void {
		this.messagePartVisible = !this.messagePartVisible;
	}

	trackContact(index: number, contact: Contact): string {
		return contact.address;
	}

	stopPropagation(event: Event): void {
		event.stopPropagation();
	}
}

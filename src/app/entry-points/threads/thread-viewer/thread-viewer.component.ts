import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReplaySubject } from 'rxjs';
import { distinctUntilChanged, filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { EmailsStorageService } from '../../../core/services/emails-storage.service';
import { RouteBuilderService } from '../../../core/services/route-builder.service';
import { IMessage, IThread } from '../../../shared/types';

@Component({
	selector: 'thread-viewer',
	templateUrl: 'thread-viewer.component.html',
	styleUrls: ['thread-viewer.component.css'],
})
export class ThreadViewerComponent implements OnInit, OnDestroy {
	private destroy: ReplaySubject<null> = new ReplaySubject(1);
	threadId = '';
	messages: IMessage[] = [];
	thread: IThread | null = null;
	loading = false;

	@ViewChild('threadContainer') threadContainer?: ElementRef<HTMLElement>;

	constructor(
		private router: Router,
		public route: ActivatedRoute,
		private emailsStorageService: EmailsStorageService,
		private routeBuilderService: RouteBuilderService,
	) {}

	ngOnInit(): void {
		this.route.paramMap
			.pipe(
				map(paramMap => paramMap.get('threadId') || ''),
				distinctUntilChanged(),
				tap(threadId => {
					this.threadId = threadId;
					if (!threadId) {
						this.messages = [];
						this.thread = null;
					}
					this.routeBuilderService.updateCurrentThreadId(threadId);
				}),
				filter(threadId => Boolean(threadId)),
				tap(() => (this.loading = true)),
				switchMap(threadId => {
					return this.emailsStorageService.loadFullThreadByIdFromServer(threadId);
				}),
				tap(fullThread => {
					if (fullThread) {
						this.thread = fullThread.thread;
						this.messages = fullThread.messages;
					}
					this.scrollThreadToTop();
					this.loading = false;
				}),
				takeUntil(this.destroy),
			)
			.subscribe();
	}

	ngOnDestroy(): void {
		this.destroy.next(null);
		this.destroy.complete();
	}

	private scrollThreadToTop(): void {
		this.threadContainer?.nativeElement.scrollTo({ top: 0 });
	}

	trackMessage(index: number, message: IMessage): string {
		return message._id;
	}
}

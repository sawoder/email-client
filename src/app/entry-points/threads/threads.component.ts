import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ReplaySubject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import {
	RouteBuilderService,
	ThreadViewRouteBuilder,
} from '../../core/services/route-builder.service';
import { SearchQueryService } from '../../core/services/search-query.service';
import { ISearchQuery } from '../../shared/types';
import { SearchQueryDialogComponent } from './search-query-dialog/search-query-dialog.component';

import search from '!!raw-loader!material-design-icons/action/svg/production/ic_search_24px.svg';
import settings from '!!raw-loader!material-design-icons/action/svg/production/ic_settings_24px.svg';
import clear from '!!raw-loader!material-design-icons/content/svg/production/ic_clear_24px.svg';

@Component({
	selector: 'app-threads',
	templateUrl: 'threads.component.html',
	styleUrls: ['threads.component.css'],
})
export class ThreadsComponent implements OnInit, OnDestroy {
	private destroy: ReplaySubject<null> = new ReplaySubject(1);
	logoImage = 'assets/logo.png';
	logoWebpImage = 'assets/logo@1x.webp';
	logoWebpRetinaImage = 'assets/logo@2x.webp';
	logoUrl = '/threads';

	searchIcon = search;
	clearIcon = clear;
	settingsIcon = settings;

	showCloseThreadControl = false;
	showResetSearchControl = false;

	constructor(
		private routeBuilderService: RouteBuilderService,
		private route: ActivatedRoute,
		private router: Router,
		private dialog: MatDialog,
		private searchQueryService: SearchQueryService,
	) {}

	threadViewRouteBuilder: ThreadViewRouteBuilder = () => '';

	ngOnInit(): void {
		this.routeBuilderService.subscribeToState();
		this.routeBuilderService.threadViewRouteBuilderObservable
			.pipe(takeUntil(this.destroy))
			.subscribe(
				threadViewRouteBuilder => (this.threadViewRouteBuilder = threadViewRouteBuilder),
			);

		this.route.data
			.pipe(takeUntil(this.destroy))
			.subscribe(({ thread }: { thread?: boolean }) => {
				this.showCloseThreadControl = !!thread;
			});

		this.searchQueryService.searchQueryObservable
			.pipe(
				map(() => this.searchQueryService.isQueryEmpty()),
				takeUntil(this.destroy),
			)
			.subscribe(isQueryEmpty => {
				this.showResetSearchControl = !isQueryEmpty;
			});
	}

	ngOnDestroy(): void {
		this.routeBuilderService.unsubscribeToState();

		this.destroy.next(null);
		this.destroy.complete();
	}

	editSearchQuery(): void {
		const dialogRef = this.dialog.open<SearchQueryDialogComponent, ISearchQuery, ISearchQuery>(
			SearchQueryDialogComponent,
			{
				hasBackdrop: true,
				closeOnNavigation: true,
				width: 'var(--x192)',
				data: this.searchQueryService.getSearchQuerySnapshot(),
			},
		);

		dialogRef
			.afterClosed()
			.pipe(takeUntil(this.destroy))
			.subscribe(result => {
				this.searchQueryService.replaceSearchQueryInRouter(this.router, result);
			});
	}
}

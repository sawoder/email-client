import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ThreadsComponent } from './threads.component';

const routes: Routes = [
	{
		path: 'threads',
		pathMatch: 'full',
		component: ThreadsComponent,
		data: {
			reuse: true,
		},
	},
	{
		path: 'threads/:page',
		pathMatch: 'full',
		component: ThreadsComponent,
		data: {
			reuse: true,
		},
	},
	{
		path: 'threads/:page/:threadId',
		pathMatch: 'full',
		component: ThreadsComponent,
		data: {
			reuse: true,
			thread: true,
		},
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class ThreadsRoutingModule {}

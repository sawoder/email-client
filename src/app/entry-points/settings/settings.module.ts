import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { UiKitModule } from '../../ui-kit/ui-kit.module';
import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';

@NgModule({
	imports: [SettingsRoutingModule, UiKitModule, ReactiveFormsModule],
	declarations: [SettingsComponent],
	exports: [SettingsComponent],
})
export class SettingsModule {}

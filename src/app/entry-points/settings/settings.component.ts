import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { SettingsService } from '../../core/services/settings.service';

import chevronLeft from '!!raw-loader!material-design-icons/navigation/svg/production/ic_chevron_left_24px.svg';

interface ISettingsFormValues {
	darkModeEnabled: boolean;
	compactViewEnabled: boolean;
	messagesGroupingEnabled: boolean;
}

@Component({
	selector: 'app-settings',
	templateUrl: 'settings.component.html',
	styleUrls: ['settings.component.css'],
})
export class SettingsComponent implements OnInit, OnDestroy {
	private destroy: ReplaySubject<null> = new ReplaySubject(1);
	backIcon = chevronLeft;
	settingsForm: FormGroup;

	constructor(private formBuilder: FormBuilder, private settingsService: SettingsService) {
		const {
			darkModeEnabled,
			compactViewEnabled,
			messagesGroupingEnabled,
		} = this.settingsService.getSettingsSnapshot();
		this.settingsForm = this.formBuilder.group({
			darkModeEnabled: [darkModeEnabled],
			compactViewEnabled: [compactViewEnabled],
			messagesGroupingEnabled: [messagesGroupingEnabled],
		});
	}

	ngOnInit(): void {
		this.settingsForm.valueChanges
			.pipe(takeUntil(this.destroy))
			.subscribe((settings: ISettingsFormValues) => {
				this.settingsService.updateDarkModeSetting(settings.darkModeEnabled);
				this.settingsService.updateCompactViewSetting(settings.compactViewEnabled);
				this.settingsService.updateMessagesGroupingSetting(
					settings.messagesGroupingEnabled,
				);
			});
	}

	ngOnDestroy(): void {
		this.destroy.next(null);
		this.destroy.complete();
	}
}

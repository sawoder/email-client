import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { ActivationEnd, Event, Router } from '@angular/router';
import { forkJoin, of, ReplaySubject } from 'rxjs';
import { catchError, filter, takeUntil, tap } from 'rxjs/operators';

import { EmailsStorageService } from './core/services/emails-storage.service';
import { SearchQueryService } from './core/services/search-query.service';
import { SettingsService } from './core/services/settings.service';
import { COMPACT_VIEW_CLASSNAME, DARK_MODE_CLASSNAME } from './shared/constants';

const isActivationEndEvent = (event: Event): event is ActivationEnd => {
	return event instanceof ActivationEnd;
};

@Component({
	selector: 'app-root',
	templateUrl: 'app.component.html',
	styleUrls: ['app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
	private destroy: ReplaySubject<null> = new ReplaySubject(1);
	initializing = true;
	initializationFailed = false;

	constructor(
		private emailsStorageService: EmailsStorageService,
		private settingsService: SettingsService,
		@Inject(DOCUMENT) private document: Document,
		private router: Router,
		private searchQueryService: SearchQueryService,
	) {}

	ngOnInit(): void {
		forkJoin([this.emailsStorageService.initialize(), this.settingsService.initialize()])
			.pipe(
				tap(() => (this.initializing = false)),
				tap(() => this.subscribeToAppearanceSettings()),
				catchError(() => {
					this.initializationFailed = true;
					this.initializing = false;
					return of(null);
				}),
				takeUntil(this.destroy),
			)
			.subscribe();

		this.searchQueryService.updateSearchQueryFromRouter(
			this.router.routerState.snapshot.root.queryParamMap,
		);

		this.router.events
			.pipe(
				filter(isActivationEndEvent),
				tap((event: ActivationEnd) => {
					this.searchQueryService.updateSearchQueryFromRouter(
						event.snapshot.queryParamMap,
					);
				}),
				takeUntil(this.destroy),
			)
			.subscribe();
	}

	ngOnDestroy(): void {
		this.destroy.next(null);
		this.destroy.complete();
	}

	subscribeToAppearanceSettings(): void {
		this.settingsService
			.darkModeObservable()
			.pipe(
				tap(darkModeEnabled => this.setDarkMode(darkModeEnabled)),
				takeUntil(this.destroy),
			)
			.subscribe();
		this.settingsService
			.compactViewObservable()
			.pipe(
				tap(compactViewEnabled => this.setCompactView(compactViewEnabled)),
				takeUntil(this.destroy),
			)
			.subscribe();
	}

	setDarkMode(enabled: boolean): void {
		this.document.documentElement.classList.toggle(DARK_MODE_CLASSNAME, enabled);
	}

	setCompactView(enabled: boolean): void {
		this.document.documentElement.classList.toggle(COMPACT_VIEW_CLASSNAME, enabled);
	}
}

import { Injectable } from '@angular/core';
import { iif, Observable, of } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { fromArray } from 'rxjs/internal/observable/fromArray';
import { delay, first, last, mapTo, mergeMap, scan, switchMap, tap } from 'rxjs/operators';

import deepCopy from 'deepcopy';
import * as stableSort from 'stable';

import {
	Contact,
	IFullThread,
	IMessage,
	IPage,
	ISearchQuery,
	IThread,
	ThreadType,
} from '../../shared/types';
import { isDate, toDayEnd, toDayStart } from '../../shared/utils/date.utils';
import { addOccurrences, findOccurrences } from '../../shared/utils/search.utils';
import { LoadEmailsService } from './load-emails.service';
import { ProcessEmailsService } from './process-emails.service';
import { SearchQueryService } from './search-query.service';
import { SettingsService } from './settings.service';

interface IThreadSearchResult {
	thread: IThread;
	searchWeight: number;
}

@Injectable()
export class EmailsStorageService {
	private messages: Map<string, IMessage> = new Map();
	private simpleThreads: Map<string, IThread> = new Map();
	private groupedThreads: Map<string, IThread> = new Map();

	private simpleThreadsDateIndex: string[] = [];
	private groupedThreadsDateIndex: string[] = [];

	constructor(
		private loadEmailsService: LoadEmailsService,
		private processEmailsService: ProcessEmailsService,
		private settingsService: SettingsService,
		private searchQueryService: SearchQueryService,
	) {}

	initialize(): Observable<true> {
		return this.loadEmailsService.loadEmailsArchive().pipe(
			switchMap(emails => {
				return iif(
					() => emails.length > 0,
					fromArray(emails).pipe(
						mergeMap(email =>
							fromPromise(this.processEmailsService.parseRawEmail(email)),
						),
						scan(
							(emails, email) => emails.set(email._id, email),
							new Map<string, IMessage>(),
						),
					),
					of(new Map<string, IMessage>()),
				);
			}),
			last(),
			tap(emailsMap => (this.messages = emailsMap)),
			tap(this.buildThreads),
			tap(this.buildIndexes),
			mapTo(true),
		);
	}

	buildSimpleThreads = (): void => {
		this.messages.forEach(message => {
			const thread = this.processEmailsService.createSimpleThread(message);
			this.simpleThreads.set(thread._id, thread);
		});
	};

	buildGroupedThreads = (): void => {
		const groupedThreads = this.processEmailsService.groupEmailsBySender(this.messages);
		for (const groupedThread of groupedThreads) {
			this.groupedThreads.set(groupedThread._id, groupedThread);
		}
	};

	buildThreads = (): void => {
		this.buildSimpleThreads();
		this.buildGroupedThreads();
	};

	buildIndexes = (): void => {
		this.simpleThreadsDateIndex = [...this.simpleThreads.values()]
			.sort((left, right) => right.date.valueOf() - left.date.valueOf())
			.map(email => email._id);

		this.groupedThreadsDateIndex = [...this.groupedThreads.values()]
			.sort((left, right) => right.date.valueOf() - left.date.valueOf())
			.map(thread => thread._id);
	};

	loadThreadsPageFromServer = (page: number): Observable<IPage<IThread>> => {
		if (page < 0) {
			throw new Error('Invalid page number ' + page);
		}
		return new Observable<IPage<IThread>>(stream => {
			const {
				pageSize,
				messagesGroupingEnabled,
			} = this.settingsService.getSettingsSnapshot();
			const isQueryEmpty = this.searchQueryService.isQueryEmpty();
			const searchQuery = this.searchQueryService.getSearchQuerySnapshot();
			const pageStartPosition = page * pageSize;

			// Filtering is possible only with messages grouping disabled
			const threadsList =
				messagesGroupingEnabled && isQueryEmpty
					? this.groupedThreadsDateIndex.map(threadId =>
							this.groupedThreads.get(threadId),
					  )
					: this.simpleThreadsDateIndex.map(threadId => this.simpleThreads.get(threadId));

			const filteredEmails = this.filterThreadsListByQuery(
				threadsList.filter((thread?: IThread): thread is IThread => Boolean(thread)),
				searchQuery,
			);

			const isEmpty = filteredEmails.length === 0;
			const outOfRange = isEmpty ? page !== 0 : filteredEmails.length - 1 < pageStartPosition;
			const hasPrevious = pageStartPosition > 0 && !outOfRange;
			const hasNext = (page + 1) * pageSize < filteredEmails.length && !outOfRange;

			const allEntitiesCount = filteredEmails.length;
			const pagesCount = Math.ceil(allEntitiesCount / pageSize);

			let threads: IThread[] = filteredEmails.slice(
				pageStartPosition,
				pageStartPosition + pageSize,
			);

			if (!isQueryEmpty) {
				threads = threads.map(thread =>
					this.highlightSearchOccurrencesInThread(thread, searchQuery),
				);
			}

			stream.next({
				page: page + 1,
				pagesCount,
				allEntitiesCount,

				hasPrevious,
				hasNext,
				outOfRange,
				isEmpty,

				entities: threads,
			});
			stream.complete();
		}).pipe(first(), delay(200 + 300 * Math.random()));
	};

	filterThreadsListByQuery = (threadsList: IThread[], searchQuery: ISearchQuery): IThread[] => {
		const filteredEmails: IThreadSearchResult[] = threadsList
			.map((thread): IThreadSearchResult | null => {
				const [matches, searchWeight] = this.isThreadFitsQuery(thread, searchQuery);
				return !matches
					? null
					: {
							thread,
							searchWeight,
					  };
			})
			.filter((threadSearchResult): threadSearchResult is IThreadSearchResult =>
				Boolean(threadSearchResult),
			);

		return stableSort(
			filteredEmails,
			(left: IThreadSearchResult, right: IThreadSearchResult): boolean =>
				left.searchWeight < right.searchWeight,
		).map(filteredEmail => filteredEmail.thread);
	};

	loadFullThreadByIdFromServer = (threadId: string): Observable<IFullThread | null> => {
		return new Observable<IFullThread | null>(stream => {
			const threadType = threadId.split(':', 1).pop();
			let thread: IThread | null = null;
			switch (threadType) {
				case ThreadType.simple:
					thread = this.simpleThreads.get(threadId) || null;
					break;
				case ThreadType.grouped:
					thread = this.groupedThreads.get(threadId) || null;
					break;
				default:
					stream.next(null);
					break;
			}
			if (thread) {
				const isQueryEmpty = this.searchQueryService.isQueryEmpty();
				const searchQuery = this.searchQueryService.getSearchQuerySnapshot();
				let messages = thread.messages.map(
					messageId => this.messages.get(messageId) as IMessage,
				);
				if (!isQueryEmpty) {
					thread = this.highlightSearchOccurrencesInThread(thread, searchQuery);
					messages = messages.map(message =>
						this.highlightSearchOccurrencesInMessage(message, searchQuery),
					);
				}
				stream.next({
					thread,
					messages,
				});
			} else {
				stream.next(null);
			}
			stream.complete();
		}).pipe(first(), delay(350 + 150 * Math.random()));
	};

	isThreadFitsQuery = (thread: IThread, searchQuery: ISearchQuery): [boolean, number] => {
		if (
			isDate(searchQuery.qDateFrom) &&
			thread.date.valueOf() < toDayStart(searchQuery.qDateFrom).valueOf()
		) {
			return [false, 0];
		}
		if (
			isDate(searchQuery.qDateTo) &&
			thread.date.valueOf() > toDayEnd(searchQuery.qDateTo).valueOf()
		) {
			return [false, 0];
		}

		const mainMessageId = thread.messages[0];
		const mainMessage = mainMessageId && this.messages.get(mainMessageId);

		if (!mainMessage) {
			return [false, 0];
		}

		if (searchQuery.qFrom && !this.hasInContacts(searchQuery.qFrom, [mainMessage.from])) {
			return [false, 0];
		}

		if (
			searchQuery.qTo &&
			!this.hasInContacts(searchQuery.qTo, [
				...mainMessage.to,
				...mainMessage.cc,
				...mainMessage.bcc,
			])
		) {
			return [false, 0];
		}

		let occurrencesCount = 0;

		if (searchQuery.qQuery) {
			const subjectOccurrences = findOccurrences(
				searchQuery.qQuery,
				mainMessage.subject.normalized.content,
			);
			const bodyOccurrences = findOccurrences(
				searchQuery.qQuery,
				mainMessage.body.raw.content,
			);

			occurrencesCount += subjectOccurrences.length + bodyOccurrences.length;

			if (subjectOccurrences.length === 0 && bodyOccurrences.length === 0) {
				return [false, 0];
			}
		}
		return [true, occurrencesCount];
	};

	highlightSearchOccurrencesInThread = (thread: IThread, searchQuery: ISearchQuery): IThread => {
		const threadCopy = deepCopy(thread);
		const { qQuery } = searchQuery;
		if (qQuery) {
			threadCopy.subject.normalized = addOccurrences(qQuery, threadCopy.subject.normalized);
			threadCopy.messageSnippet.snippet = addOccurrences(
				qQuery,
				threadCopy.messageSnippet.snippet,
			);
		}

		return threadCopy;
	};

	highlightSearchOccurrencesInMessage = (
		message: IMessage,
		searchQuery: ISearchQuery,
	): IMessage => {
		const messageCopy = deepCopy(message);
		const { qQuery } = searchQuery;
		if (qQuery) {
			messageCopy.subject.normalized = addOccurrences(qQuery, messageCopy.subject.normalized);
			messageCopy.snippet.snippet = addOccurrences(qQuery, messageCopy.snippet.snippet);
			messageCopy.body.raw = addOccurrences(qQuery, messageCopy.body.raw);
			messageCopy.body.message = addOccurrences(qQuery, messageCopy.body.message);
			if (messageCopy.body.citation) {
				messageCopy.body.citation = addOccurrences(qQuery, messageCopy.body.citation);
			}
			if (messageCopy.body.forwarding) {
				messageCopy.body.forwarding = addOccurrences(qQuery, messageCopy.body.forwarding);
			}
		}

		return messageCopy;
	};

	private hasInContacts(query: string, contacts: Contact[]): boolean {
		return contacts.some(contact => {
			return contact.address.toLowerCase().includes(query.toLowerCase());
		});
	}
}

import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable, ReplaySubject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

export type PageViewRouteBuilder = (pageNumber: number) => string;
export type ThreadViewRouteBuilder = (threadId?: string) => string;

const emptyBuilder = (): string => '';

interface CurrentRouteState {
	pageNumber?: number;
	threadId?: string;
}

@Injectable()
export class RouteBuilderService implements OnDestroy {
	private destroy: ReplaySubject<null> = new ReplaySubject(1);

	pageViewRouteBuilderObservable: Observable<PageViewRouteBuilder>;
	threadViewRouteBuilderObservable: Observable<ThreadViewRouteBuilder>;
	private routeStateSubscription: Subscription | null = null;

	private currentRouteState: BehaviorSubject<CurrentRouteState> = new BehaviorSubject({});
	private pageViewRouteBuilder: BehaviorSubject<PageViewRouteBuilder> = new BehaviorSubject(
		emptyBuilder as PageViewRouteBuilder,
	);
	private threadViewRouteBuilder: BehaviorSubject<ThreadViewRouteBuilder> = new BehaviorSubject(
		emptyBuilder as ThreadViewRouteBuilder,
	);

	constructor() {
		this.pageViewRouteBuilderObservable = this.pageViewRouteBuilder.asObservable();
		this.threadViewRouteBuilderObservable = this.threadViewRouteBuilder.asObservable();
	}

	subscribeToState(): void {
		this.routeStateSubscription = this.currentRouteState
			.pipe(takeUntil(this.destroy))
			.subscribe(() => this.updateBuilders());
	}

	unsubscribeToState(): void {
		if (this.routeStateSubscription) {
			this.routeStateSubscription.unsubscribe();
		}
		this.routeStateSubscription = null;
	}

	updateCurrentPage(pageNumber: number | undefined): void {
		const newState: CurrentRouteState = {
			...this.currentRouteState.getValue(),
			pageNumber,
		};

		this.currentRouteState.next(newState);
	}

	updateCurrentThreadId(threadId: string | undefined): void {
		const newState: CurrentRouteState = {
			...this.currentRouteState.getValue(),
			threadId,
		};

		this.currentRouteState.next(newState);
	}

	private getPageViewRouteBuilder({ threadId }: CurrentRouteState): PageViewRouteBuilder {
		return (pageNumber: number): string => {
			if (threadId) {
				return `/threads/${pageNumber}/${threadId}`;
			}
			return `/threads/${pageNumber}`;
		};
	}

	private getThreadViewRouteBuilder({ pageNumber }: CurrentRouteState): ThreadViewRouteBuilder {
		return (threadId?: string): string => {
			if (!threadId) {
				return `/threads/${pageNumber === 1 ? '' : pageNumber || 1}`;
			}
			return `/threads/${pageNumber || 1}/${threadId}`;
		};
	}

	private updateBuilders(): void {
		const currentState: CurrentRouteState = this.currentRouteState.getValue();

		this.pageViewRouteBuilder.next(this.getPageViewRouteBuilder(currentState));
		this.threadViewRouteBuilder.next(this.getThreadViewRouteBuilder(currentState));
	}

	ngOnDestroy(): void {
		this.destroy.next(null);
		this.destroy.complete();

		this.currentRouteState.complete();
		this.pageViewRouteBuilder.complete();
		this.threadViewRouteBuilder.complete();
	}
}

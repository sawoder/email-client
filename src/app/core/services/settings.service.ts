import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged, map, mapTo, tap } from 'rxjs/operators';

import { LOCAL_STORAGE, StorageService, StorageTranscoders } from 'ngx-webstorage-service';

import { STORAGE_KEY } from '../../shared/constants';
import { ISettings } from '../../shared/types';

const defaultSettings: ISettings = {
	pageSize: 25,
	darkModeEnabled: false,
	compactViewEnabled: false,
	messagesGroupingEnabled: false,
};

@Injectable()
export class SettingsService {
	private settings: BehaviorSubject<ISettings> = new BehaviorSubject(defaultSettings);

	constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {}

	initialize(): Observable<true> {
		return new Observable<ISettings>(stream => {
			try {
				const settings = this.storage.get(STORAGE_KEY, StorageTranscoders.JSON) || {};
				stream.next({
					...defaultSettings,
					...settings,
				});
			} catch {
				stream.next(defaultSettings);
			}
			stream.complete();
		}).pipe(
			tap(settings => this.settings.next(settings)),
			mapTo(true),
		);
	}

	getSettingsSnapshot = (): ISettings => {
		return this.settings.getValue();
	};

	updateDarkModeSetting = (darkModeEnabled: boolean): void => {
		const newSnapshot: ISettings = {
			...this.getSettingsSnapshot(),
			darkModeEnabled,
		};

		try {
			this.storage.set(STORAGE_KEY, newSnapshot, StorageTranscoders.JSON);
		} finally {
			this.settings.next(newSnapshot);
		}
	};
	updateCompactViewSetting = (compactViewEnabled: boolean): void => {
		const newSnapshot: ISettings = {
			...this.getSettingsSnapshot(),
			compactViewEnabled,
		};

		try {
			this.storage.set(STORAGE_KEY, newSnapshot, StorageTranscoders.JSON);
		} finally {
			this.settings.next(newSnapshot);
		}
	};
	updateMessagesGroupingSetting = (messagesGroupingEnabled: boolean): void => {
		const newSnapshot: ISettings = {
			...this.getSettingsSnapshot(),
			messagesGroupingEnabled,
		};

		try {
			this.storage.set(STORAGE_KEY, newSnapshot, StorageTranscoders.JSON);
		} finally {
			this.settings.next(newSnapshot);
		}
	};

	darkModeObservable = (): Observable<boolean> => {
		return this.settings.asObservable().pipe(
			map(settings => settings.darkModeEnabled),
			distinctUntilChanged(),
		);
	};

	compactViewObservable = (): Observable<boolean> => {
		return this.settings.asObservable().pipe(
			map(settings => settings.compactViewEnabled),
			distinctUntilChanged(),
		);
	};
}

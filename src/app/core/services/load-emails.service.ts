import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { IRawEmail } from '../../shared/types';

@Injectable()
export class LoadEmailsService {
	emailsUrl = 'assets/emails.json';
	emptyEmailsUrl = 'assets/empty-emails.json'; // for debug with empty response

	constructor(private http: HttpClient) {}

	loadEmailsArchive(): Observable<IRawEmail[]> {
		return this.http
			.get<IRawEmail[]>(this.emailsUrl)
			.pipe(retry(1), catchError(this.handleError));
	}

	// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
	private handleError = (error: HttpErrorResponse) => {
		if (error.error instanceof ErrorEvent) {
			// A client-side or network error occurred. Handle it accordingly.
			console.error('An error occurred:', error.error.message);
		} else {
			// The backend returned an unsuccessful response code.
			// The response body may contain clues as to what went wrong,
			console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
		}
		// return an observable with a user-facing error message
		return throwError('Something bad happened; please try again later');
	};
}

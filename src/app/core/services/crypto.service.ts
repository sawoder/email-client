import { Inject, Injectable, InjectionToken } from '@angular/core';

import { v4 as uuid } from 'uuid';

export const CRYPTO_API = new InjectionToken<Crypto>('Crypto Web API', {
	factory: (): Crypto => window.crypto,
});

@Injectable()
export class CryptoService {
	constructor(@Inject(CRYPTO_API) private crypto: Crypto) {}

	async calculateHashSum(message: string): Promise<string> {
		const encoder = new TextEncoder();
		const data = encoder.encode(message);
		const hash = await this.crypto.subtle.digest('SHA-256', data);
		const uint8Array = new Uint8Array(hash);
		return uuid({ random: uint8Array });
	}
}

import { Injectable } from '@angular/core';
import { ParamMap, Params, Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

import { ISearchQuery } from '../../shared/types';
import { isDate, isDatesSame, parseDate } from '../../shared/utils/date.utils';

@Injectable()
export class SearchQueryService {
	searchQueryObservable: Observable<ISearchQuery>;
	private searchQuery: BehaviorSubject<ISearchQuery> = new BehaviorSubject({});

	constructor() {
		this.searchQueryObservable = this.searchQuery.asObservable();
	}

	getSearchQuerySnapshot = (): ISearchQuery => {
		return this.searchQuery.getValue();
	};

	serialize({ qDateTo, qDateFrom, ...result }: ISearchQuery): Params {
		const params: Params = Object.entries(result).reduce((p: Params, [key, value]) => {
			if (value) {
				p[key] = String(value);
			}
			return p;
		}, {} as Params);
		if (qDateTo instanceof Date) {
			params.qDateTo = qDateTo.toISOString();
		}
		if (qDateFrom instanceof Date) {
			params.qDateFrom = qDateFrom.toISOString();
		}

		return params;
	}

	parse(params: ParamMap): ISearchQuery {
		const query: ISearchQuery = {};

		const qQuery = params.get('qQuery')?.trim();
		const qFrom = params.get('qFrom')?.trim();
		const qTo = params.get('qTo')?.trim();
		const qDateFrom = parseDate(params.get('qDateFrom'));
		const qDateTo = parseDate(params.get('qDateTo'));

		if (qQuery) {
			query.qQuery = qQuery;
		}

		if (qFrom) {
			query.qFrom = qFrom;
		}

		if (qTo) {
			query.qTo = qTo;
		}

		if (isDate(qDateFrom)) {
			query.qDateFrom = qDateFrom;
		}

		if (isDate(qDateTo)) {
			query.qDateTo = qDateTo;
		}

		return query;
	}

	replaceSearchQueryInRouter = (router: Router, newSearchQuery?: ISearchQuery): void => {
		if (!newSearchQuery) {
			return;
		}

		router.navigate(['/threads'], {
			queryParams: this.serialize(newSearchQuery),
		});
	};

	updateSearchQueryFromRouter = (params: ParamMap): void => {
		const query = this.parse(params);
		this.searchQuery.next(query);
	};

	isQueryEmpty = (): boolean => {
		const query = this.getSearchQuerySnapshot();
		return Object.keys(query).length === 0;
	};

	isQueriesSame = (queryA: ISearchQuery, queryB: ISearchQuery): boolean => {
		return (
			queryA.qQuery === queryB.qQuery &&
			queryA.qTo === queryB.qTo &&
			queryA.qFrom === queryB.qFrom &&
			isDatesSame(queryA.qDateFrom, queryB.qDateFrom) &&
			isDatesSame(queryA.qDateTo, queryB.qDateTo)
		);
	};
}

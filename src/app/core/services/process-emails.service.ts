import { Injectable } from '@angular/core';

import {
	BODY_CITATION_RE,
	BODY_FORWARDING_RE,
	SNIPPET_LENGTH,
	SUBJECT_PREFIX_FWD,
	SUBJECT_PREFIX_RE,
	SUBJECT_PREFIXES_RE,
} from '../../shared/constants';
import {
	Contact,
	IEmailBody,
	IEmailSubject,
	IMessage,
	IMessageSnippet,
	IRawEmail,
	IThread,
	SubjectActionType,
	ThreadType,
} from '../../shared/types';
import { CryptoService } from './crypto.service';

@Injectable()
export class ProcessEmailsService {
	constructor(private cryptoService: CryptoService) {}

	parseContact = (address: string): Contact => {
		return {
			address,
			avatarUrl: `https://robohash.org/${address}?size=96x96&bgset=bg2`,
		};
	};

	parseSubject = (subject: string): IEmailSubject => {
		const matches = SUBJECT_PREFIXES_RE.exec(subject);
		let normalized = subject;
		let actions: SubjectActionType[] = [];

		if (matches && matches[0].length) {
			normalized = subject.slice(matches[0].length).trim();
			actions = matches[0]
				.split(/\s/)
				.map(prefix => {
					if (SUBJECT_PREFIX_RE.test(prefix)) {
						return SubjectActionType.reply;
					}
					if (SUBJECT_PREFIX_FWD.test(prefix)) {
						return SubjectActionType.forward;
					}

					return null;
				})
				.filter((item): item is SubjectActionType => Boolean(item));
		}

		const isEmpty = normalized.length === 0;

		return {
			raw: subject,
			normalized: {
				content: normalized,
			},
			actions,
			isEmpty,
		};
	};

	parseBody = (body: string): IEmailBody => {
		let messageContent = body;
		let citationMatch = BODY_CITATION_RE.exec(messageContent);
		let forwardingMatch = BODY_FORWARDING_RE.exec(messageContent);

		if (citationMatch && forwardingMatch) {
			const firstIndex = Math.min(citationMatch.index || 0, forwardingMatch.index || 0);
			if (citationMatch.index !== firstIndex) {
				citationMatch = null;
			}
			if (forwardingMatch.index !== firstIndex) {
				forwardingMatch = null;
			}
		}

		const emailBody: IEmailBody = {
			raw: {
				content: body,
			},
			message: {
				content: '',
			},
		};

		if (citationMatch) {
			emailBody.citation = {
				content: messageContent.slice(citationMatch.index),
			};
			messageContent = messageContent.slice(0, citationMatch.index);
		}

		if (forwardingMatch && typeof forwardingMatch[1] === 'string') {
			emailBody.forwarding = {
				content: messageContent.slice(forwardingMatch.index),
				caption: forwardingMatch[1],
			};
			messageContent = messageContent.slice(0, forwardingMatch.index);
		}

		emailBody.message.content = messageContent;

		return emailBody;
	};

	parseRawEmail = async (rawEmail: IRawEmail): Promise<IMessage> => {
		const _id = await this.cryptoService.calculateHashSum(
			JSON.stringify([rawEmail.subject, rawEmail.from, rawEmail.date]),
		);

		const body = this.parseBody(rawEmail.body);

		return {
			_id,

			from: this.parseContact(rawEmail.from),
			to: rawEmail.to.map(this.parseContact),
			cc: rawEmail.cc.map(this.parseContact),
			bcc: rawEmail.bcc.map(this.parseContact),

			date: new Date(rawEmail.date),

			subject: this.parseSubject(rawEmail.subject),
			body,
			snippet: this.extractMessageSnippet(body),
		};
	};

	extractMessageSnippet = (body: IEmailBody): IMessageSnippet => {
		return {
			snippet: {
				content: body.message.content.slice(0, SNIPPET_LENGTH),
			},
		};
	};

	createSimpleThread = (message: IMessage): IThread => {
		return {
			_id: ThreadType.simple + ':' + message._id,
			from: message.from,
			subject: {
				raw: message.subject.raw,
				normalized: {
					content: message.subject.raw,
				},
				isEmpty: message.subject.raw.trim().length === 0,
				actions: [],
			},
			date: message.date,
			messageSnippet: message.snippet,

			messages: [message._id],
		};
	};

	createGroupedThread = (messages: IMessage[]): IThread => {
		const sortedByDateFromNewest = messages.sort(
			(left, right) => right.date.valueOf() - left.date.valueOf(),
		);

		const latestMessage = sortedByDateFromNewest[0];

		return {
			_id: ThreadType.grouped + ':' + latestMessage._id,
			from: latestMessage.from,

			subject: {
				...latestMessage.subject,
			},
			date: latestMessage.date,
			messageSnippet: latestMessage.snippet,

			messages: sortedByDateFromNewest.map(message => message._id),
		};
	};

	groupEmailsBySender = (messages: Map<string, IMessage>): IThread[] => {
		const groupedThreads: IThread[] = [];

		const bySubjectMap: Map<string, IMessage[]> = new Map();

		for (const message of messages.values()) {
			const normalizedSubject = message.subject.normalized.content;

			const messages = bySubjectMap.get(normalizedSubject) || [];
			messages.push(message);

			bySubjectMap.set(normalizedSubject, messages);
		}

		for (const messagesGroup of bySubjectMap.values()) {
			groupedThreads.push(this.createGroupedThread(messagesGroup));
		}

		return groupedThreads;
	};
}

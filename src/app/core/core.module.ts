import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouteReuseStrategy } from '@angular/router';

import { CustomRouteReuseStrategy } from './custom.router-reuse-strategy';
import { CryptoService } from './services/crypto.service';
import { EmailsStorageService } from './services/emails-storage.service';
import { LoadEmailsService } from './services/load-emails.service';
import { ProcessEmailsService } from './services/process-emails.service';
import { RouteBuilderService } from './services/route-builder.service';
import { SearchQueryService } from './services/search-query.service';
import { SettingsService } from './services/settings.service';

@NgModule({
	imports: [HttpClientModule],
	providers: [
		LoadEmailsService,
		ProcessEmailsService,
		EmailsStorageService,
		SettingsService,
		SearchQueryService,
		CryptoService,
		RouteBuilderService,
		{
			provide: RouteReuseStrategy,
			useClass: CustomRouteReuseStrategy,
		},
	],
})
export class CoreModule {}

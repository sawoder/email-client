import { ActivatedRouteSnapshot, DetachedRouteHandle, RouteReuseStrategy } from '@angular/router';

export class CustomRouteReuseStrategy implements RouteReuseStrategy {
	shouldDetach(): boolean {
		return false;
	}

	store(): void {}

	shouldAttach(): boolean {
		return false;
	}

	retrieve(): DetachedRouteHandle | null {
		return null;
	}

	shouldReuseRoute(future: ActivatedRouteSnapshot, current: ActivatedRouteSnapshot): boolean {
		/** We only want to reuse the route if the data of the route config contains a reuse true boolean */
		let reUseUrl = false;

		if (current?.routeConfig?.data?.reuse && future?.routeConfig?.data?.reuse) {
			reUseUrl = true;
		}

		/**
		 * Default reuse strategy by angular asserts based on the following condition
		 *
		 * @see https://github.com/angular/angular/blob/master/packages/router/src/route_reuse_strategy.ts
		 */
		const defaultReuse = future.routeConfig === current.routeConfig;

		// If either of our reuseUrl and default Url are true, we want to reuse the route
		return reUseUrl || defaultReuse;
	}
}

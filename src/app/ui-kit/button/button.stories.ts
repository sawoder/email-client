import { ButtonComponent } from './button.component';
import { moduleMetadata } from '@storybook/angular';
import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { HttpClientModule } from '@angular/common/http';
import { IconComponent } from '../icon/icon.component';
import { TextComponent } from '../text/text.component';
import { RouterModule } from '@angular/router';

import search from '!!raw-loader!material-design-icons/action/svg/production/ic_search_24px.svg';
import trash from '!!raw-loader!material-design-icons/action/svg/production/ic_delete_24px.svg';
import settings from '!!raw-loader!material-design-icons/action/svg/production/ic_settings_24px.svg';

export default {
	title: 'Button',
	component: ButtonComponent,
	decorators: [
		moduleMetadata({
			declarations: [ButtonComponent, IconComponent, TextComponent],
			imports: [
				BrowserModule,
				CommonModule,
				HttpClientModule,
				RouterModule.forRoot([{ path: '**', component: TextComponent }]),
				AngularSvgIconModule.forRoot(),
			],
			providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
		}),
	],
};

export const ButtonBase = () => ({
	component: ButtonComponent,
	props: {
		search,
		trash,
		settings,
	},
	template: `
		<div style="padding: 24px; max-width: 480px;">
			<span style="margin-right: 8px"><ui-button color="primary">Submit</ui-button></span>
			<ui-button [icon]="settings"></ui-button>
			<ui-button [icon]="trash" block>Block Button</ui-button>
		</div>
		<div style="padding: 24px; max-width: 480px; background-color: #333">
			<span style="margin-right: 8px"><ui-button [icon]="search" color="dark">Always Dark Button</ui-button></span>
			<span style="margin-right: 8px"><ui-button [icon]="search" color="light">Always Light Button</ui-button></span>
		</div>
	`,
});

ButtonBase.story = {
	name: 'examples',
};

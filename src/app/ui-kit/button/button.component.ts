import {
	AfterViewInit,
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	ElementRef,
	Input,
	OnDestroy,
	OnInit,
	QueryList,
	ViewChild,
	ViewChildren,
} from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { startWith, takeUntil } from 'rxjs/operators';

type ButtonColor = 'primary' | 'secondary' | 'dark' | 'light';

@Component({
	selector: 'ui-button',
	templateUrl: 'button.component.html',
	styleUrls: ['button.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonComponent implements OnInit, OnDestroy, AfterViewInit {
	private destroy: ReplaySubject<null> = new ReplaySubject(1);
	@Input() color?: ButtonColor = 'secondary';
	@Input() icon?: string;
	@Input() block?: boolean | string;
	@Input() href?: string;
	hasContentProjection = true;
	@ViewChildren('contentProjection') contentProjectionWrapper?: QueryList<
		ElementRef<HTMLElement>
	>;
	@ViewChild('contentProjection') contentProjection?: ElementRef<HTMLElement>;

	constructor(private changeDetectorRef: ChangeDetectorRef) {}

	ngOnInit(): void {}

	ngOnDestroy(): void {
		this.destroy.next(null);
		this.destroy.complete();
	}

	buttonClasses(): Record<string, boolean> {
		return {
			base: true,
			primary: this.color === 'primary',
			secondary: this.color === 'secondary',
			dark: this.color === 'dark',
			light: this.color === 'light',

			block: Boolean(this.block || this.block === ''),
			onlyIcon: !this.hasContentProjection,
		};
	}

	iconClasses(): Record<string, boolean> {
		return {
			icon: true,
			onlyIcon: !this.hasContentProjection,
		};
	}

	updateContentProjection(): void {
		if (!this.contentProjection) {
			this.hasContentProjection = false;
			return;
		}
		const detailContentProjection = this.contentProjection.nativeElement.childNodes;
		for (let x = 0; x < detailContentProjection.length; x++) {
			// nodeType == Node.COMMENT_NODE (8) means is a comment
			if (detailContentProjection.item(x).nodeType !== Node.COMMENT_NODE) {
				this.hasContentProjection = true;
				return;
			}
		}
		this.hasContentProjection = false;
	}

	ngAfterViewInit(): void {
		this.contentProjectionWrapper?.changes
			.pipe(startWith(null), takeUntil(this.destroy))
			.subscribe(() => {
				this.updateContentProjection();
				this.changeDetectorRef.detectChanges();
			});
	}
}

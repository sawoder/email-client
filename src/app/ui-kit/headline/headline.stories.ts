import { HeadlineComponent } from './headline.component';
import { moduleMetadata } from '@storybook/angular';
import { LogoComponent } from '../logo/logo.component';
import { ButtonComponent } from '../button/button.component';
import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { IconComponent } from '../icon/icon.component';
import { TextComponent } from '../text/text.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { RouterModule } from '@angular/router';

import settings from '!!raw-loader!material-design-icons/action/svg/production/ic_settings_24px.svg';

export default {
	title: 'Headline',
	component: HeadlineComponent,
	decorators: [
		moduleMetadata({
			declarations: [
				HeadlineComponent,
				LogoComponent,
				ButtonComponent,
				IconComponent,
				TextComponent,
			],
			imports: [
				BrowserModule,
				CommonModule,
				HttpClientModule,
				RouterModule.forRoot([{ path: '**', component: TextComponent }]),
				AngularSvgIconModule.forRoot(),
			],
			providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
		}),
	],
};

export const HeadlineBase = () => ({
	component: HeadlineComponent,
	props: {
		imageSrc: 'assets/logo.png',
		webpSrc: 'assets/logo@1x.webp',
		webpRetinaSrc: 'assets/logo@2x.webp',
		url: '/',
		settings,
	},
	template: `
		<ui-headline>
			<ui-logo [imageSrc]="imageSrc" [webpSrc]="webpSrc" [webpRetinaSrc]="webpRetinaSrc" [url]="url">Company</ui-logo>
			<ui-button [icon]="settings" color="dark" style="margin-left: auto">Settings</ui-button>
		</ui-headline>
	`,
});

HeadlineBase.story = {
	name: 'base view',
};

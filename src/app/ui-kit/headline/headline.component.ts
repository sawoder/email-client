import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
	selector: 'ui-headline',
	templateUrl: 'headline.component.html',
	styleUrls: ['headline.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeadlineComponent implements OnInit {
	constructor() {}

	ngOnInit(): void {}
}

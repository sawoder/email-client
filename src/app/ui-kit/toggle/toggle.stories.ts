import { ToggleComponent } from './toggle.component';
import { moduleMetadata } from '@storybook/angular';
import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { HttpClientModule } from '@angular/common/http';
import { IconComponent } from '../icon/icon.component';
import { TextComponent } from '../text/text.component';
import { RouterModule } from '@angular/router';
import { FormControl, ReactiveFormsModule } from '@angular/forms';

export default {
	title: 'Toggle',
	component: ToggleComponent,
	decorators: [
		moduleMetadata({
			declarations: [ToggleComponent, TextComponent],
			imports: [BrowserModule, CommonModule, ReactiveFormsModule],
			providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
		}),
	],
};

export const ToggleBase = () => ({
	component: ToggleComponent,
	props: {
		firstCheckbox: true,
		secondCheckbox: false,
		thirdCheckbox: new FormControl(true),
	},
	template: `
		<div style="padding: 24px; max-width: 480px;">
			<ui-toggle [(ngModel)]="firstCheckbox" style="margin-bottom: 4px">First toggle</ui-toggle> <br>
			<ui-toggle [(ngModel)]="secondCheckbox" style="margin-bottom: 4px">Second toggle</ui-toggle> <br>
			<ui-toggle [formControl]="thirdCheckbox">Third toggle</ui-toggle> <br> <br>
			<code>| first = \{{firstCheckbox}\} |</code>
			<code>| second = \{{secondCheckbox}\} |</code>
			<code>| third = \{{thirdCheckbox.value}\} |</code>
		</div>
	`,
});

ToggleBase.story = {
	name: 'example',
};

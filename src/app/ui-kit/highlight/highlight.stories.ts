import { HighlightComponent } from './highlight.component';
import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { TextComponent } from '../text/text.component';
import { findOccurrences } from '../../shared/utils/search.utils';

export default {
	title: 'Highlight',
	component: HighlightComponent,
	decorators: [
		moduleMetadata({
			declarations: [HighlightComponent, TextComponent],
			imports: [BrowserModule, CommonModule],
		}),
	],
};

const content =
	'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem, doloribus facilis fugiat, harum, ipsa ipsum modi nemo non numquam quibusdam quidem quod rerum tempore voluptas!';
const occurrences = findOccurrences('lorem adip ipisicing conse ctetur icin', content);

export const HighlightBase = () => ({
	component: HighlightComponent,
	props: {
		content,
		occurrences,
	},
	template: `
	<div style="max-width: 768px">
		<div style="padding: 16px">
			<ui-text mode="header"><ui-highlight [content]="content" [occurrences]="occurrences"></ui-highlight></ui-text>
			<ui-text><ui-highlight [content]="content" [occurrences]="occurrences"></ui-highlight></ui-text>
		</div>
		<div style="padding: 16px; background-color: #fff">
			<ui-text mode="header" color="dark"><ui-highlight [content]="content" [occurrences]="occurrences"></ui-highlight></ui-text>
			<ui-text color="dark"><ui-highlight [content]="content" [occurrences]="occurrences"></ui-highlight></ui-text>
		</div>
		<div style="padding: 16px; background-color: #333">
			<ui-text mode="header" color="light"><ui-highlight [content]="content" [occurrences]="occurrences"></ui-highlight></ui-text>
			<ui-text color="light"><ui-highlight [content]="content" [occurrences]="occurrences"></ui-highlight></ui-text>
		</div>
	</div>
	`,
});

HighlightBase.story = {
	name: 'example',
};

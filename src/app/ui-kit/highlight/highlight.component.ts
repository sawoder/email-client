import { DOCUMENT } from '@angular/common';
import {
	ChangeDetectionStrategy,
	Component,
	ElementRef,
	Inject,
	Input,
	OnInit,
} from '@angular/core';

type ILocalOccurrences = Array<[number, number]>;

@Component({
	selector: 'ui-highlight',
	template: '',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HighlightComponent implements OnInit {
	private componentContent = '';
	private componentOccurrences: ILocalOccurrences = [];

	constructor(
		private self: ElementRef<HTMLElement>,
		@Inject(DOCUMENT) private document: Document,
	) {}

	@Input()
	set content(value: string) {
		this.componentContent = value;
		this.highlight();
	}

	@Input()
	set occurrences(value: ILocalOccurrences | undefined | null) {
		this.componentOccurrences = value || [];
		this.highlight();
	}

	ngOnInit(): void {
		this.highlight();
	}

	private highlight(): void {
		const { componentContent, componentOccurrences, document } = this;
		const { nativeElement } = this.self;

		while (nativeElement.firstChild) {
			nativeElement.removeChild(nativeElement.firstChild);
		}

		const fragment = document.createDocumentFragment();
		let beginIndex = 0;
		for (const occurrence of componentOccurrences) {
			if (beginIndex < occurrence[0]) {
				fragment.appendChild(
					document.createTextNode(componentContent.slice(beginIndex, occurrence[0])),
				);
			}
			const mark = document.createElement('mark');
			mark.textContent = componentContent.slice(occurrence[0], occurrence[1]);
			fragment.appendChild(mark);
			beginIndex = occurrence[1];
		}

		if (beginIndex < componentContent.length) {
			fragment.appendChild(document.createTextNode(componentContent.slice(beginIndex)));
		}

		nativeElement.appendChild(fragment);
	}
}

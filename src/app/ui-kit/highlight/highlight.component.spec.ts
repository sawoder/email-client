import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HighlightComponent } from './highlight.component';
import { findOccurrences } from '../../shared/utils/search.utils';

const shortTestMessage = 'Test message';
const shortQuery = 'test age';
const largeTestMessage =
	'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem, doloribus facilis fugiat, harum, ipsa ipsum modi nemo non numquam quibusdam quidem quod rerum tempore voluptas!';
const largeQuery = 'lorem adip ipisicing conse ctetur icin';

describe('HighlightComponent', () => {
	let component: HighlightComponent;
	let fixture: ComponentFixture<HighlightComponent>;

	beforeEach(() => {
		TestBed.configureTestingModule({
			declarations: [HighlightComponent],
		});
		fixture = TestBed.createComponent(HighlightComponent);
		component = fixture.componentInstance;
	});

	it('should create', () => {
		expect(component).toBeDefined();
	});

	it('should render textContent', () => {
		const element: HTMLElement = fixture.nativeElement;
		component.content = shortTestMessage;
		expect(element.textContent).toContain(shortTestMessage);
	});

	it('should highlight occurrences', () => {
		const element: HTMLElement = fixture.nativeElement;
		component.content = shortTestMessage;
		component.occurrences = findOccurrences(shortQuery, shortTestMessage);
		expect(element.textContent).toContain(shortTestMessage);
		expect(element.innerHTML).toMatchSnapshot();
	});

	it('should rerender when props changes', () => {
		const element: HTMLElement = fixture.nativeElement;
		component.content = shortTestMessage;
		component.occurrences = findOccurrences(shortQuery, shortTestMessage);
		expect(element.textContent).toContain(shortTestMessage);
		component.content = largeTestMessage;
		component.occurrences = findOccurrences(largeQuery, largeTestMessage);
		expect(element.textContent).toContain(largeTestMessage);
		expect(element.innerHTML).toMatchSnapshot();
	});
});

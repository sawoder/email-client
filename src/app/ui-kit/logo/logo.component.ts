import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'ui-logo',
	templateUrl: 'logo.component.html',
	styleUrls: ['logo.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LogoComponent implements OnInit {
	@Input() imageSrc = '';
	@Input() webpSrc = '';
	@Input() webpRetinaSrc = '';
	@Input() url = '';

	constructor() {}

	ngOnInit(): void {}
}

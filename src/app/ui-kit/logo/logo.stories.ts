import { LogoComponent } from './logo.component';
import { moduleMetadata } from '@storybook/angular';
import { HeadlineComponent } from '../headline/headline.component';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { TextComponent } from '../text/text.component';

export default {
	title: 'Logo',
	component: LogoComponent,
	decorators: [
		moduleMetadata({
			declarations: [LogoComponent, HeadlineComponent, TextComponent],
			imports: [
				BrowserModule,
				CommonModule,
				RouterModule.forRoot([{ path: '**', component: TextComponent }]),
			],
			providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
		}),
	],
};

export const LogoBase = () => ({
	component: LogoComponent,
	props: {
		imageSrc: 'assets/logo.png',
		webpSrc: 'assets/logo@1x.webp',
		webpRetinaSrc: 'assets/logo@2x.webp',
		url: '/',
	},
	template: `
		<ui-headline>
			<ui-logo
				[imageSrc]="imageSrc"
				[webpSrc]="webpSrc"
				[webpRetinaSrc]="webpRetinaSrc"
				[url]="url"
			>Company</ui-logo>
		</ui-headline>
	`,
});

LogoBase.story = {
	name: 'base view',
};

export const LogoCompact = () => ({
	component: LogoComponent,
	props: {
		imageSrc: 'assets/logo.png',
		webpSrc: 'assets/logo@1x.webp',
		webpRetinaSrc: 'assets/logo@2x.webp',
		url: '/',
	},
	template: `
		<div class="dangerously-compact">
			<ui-headline>
				<ui-logo
					[imageSrc]="imageSrc"
					[webpSrc]="webpSrc"
					[webpRetinaSrc]="webpRetinaSrc"
					[url]="url"
				>Company</ui-logo>
			</ui-headline>
		</div>
	`,
});

LogoCompact.story = {
	name: 'compact view',
};

# ui-kit

I have created a small ui-kit to complete the test task and to demonstrate my skills. Components can be displayed in a dark theme and in a compact view.

For easy development I've integrated [Storybook](https://storybook.js.org/) into the project.

You can start it with the `npm run storybook` command

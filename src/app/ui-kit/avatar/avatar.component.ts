import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

type AvatarSize = 'large' | 'small';

@Component({
	selector: 'ui-avatar',
	templateUrl: 'avatar.component.html',
	styleUrls: ['avatar.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AvatarComponent implements OnInit {
	@Input() src = '';
	@Input() size: AvatarSize = 'large';

	constructor() {}

	ngOnInit(): void {}
}

import { AvatarComponent } from './avatar.component';
import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

export default {
	title: 'Avatar',
	component: AvatarComponent,
	decorators: [
		moduleMetadata({
			declarations: [AvatarComponent],
			imports: [BrowserModule, CommonModule],
		}),
	],
};

const avatarUrls = [
	'https://robohash.org/alex@gmail.com?size=96x96&bgset=bg2',
	'https://robohash.org/jeremy@gmail.com?size=96x96&bgset=bg2',
	'https://robohash.org/susan@gmail.com?size=96x96&bgset=bg2',
];

export const AvatarBase = () => ({
	component: AvatarComponent,
	props: {
		avatarUrls,
	},
	template: `
	<div style="padding: 16px; max-width: 480px">
		<ui-avatar *ngFor="let avatarUrl of avatarUrls" [src]="avatarUrl"></ui-avatar>
		<br>
		<ui-avatar *ngFor="let avatarUrl of avatarUrls" [src]="avatarUrl" size="small"></ui-avatar>
	</div>
	`,
});

AvatarBase.story = {
	name: 'examples',
};

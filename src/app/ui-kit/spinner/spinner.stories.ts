import { SpinnerComponent } from './spinner.component';
import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

export default {
	title: 'Spinner',
	component: SpinnerComponent,
	decorators: [
		moduleMetadata({
			declarations: [SpinnerComponent],
			imports: [BrowserModule, CommonModule],
		}),
	],
};

export const SpinnerBase = () => ({
	component: SpinnerComponent,
	props: {},
	template: `
	<div style="padding: 32px; max-width: 480px">

		<ui-spinner></ui-spinner>

	</div>
	`,
});

SpinnerBase.story = {
	name: 'base view',
};

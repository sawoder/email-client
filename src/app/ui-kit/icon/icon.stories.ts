import { IconComponent } from './icon.component';
import { moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { HttpClientModule } from '@angular/common/http';

import search from '!!raw-loader!material-design-icons/action/svg/production/ic_search_24px.svg';
import trash from '!!raw-loader!material-design-icons/action/svg/production/ic_delete_24px.svg';
import settings from '!!raw-loader!material-design-icons/action/svg/production/ic_settings_24px.svg';

export default {
	title: 'Icon',
	component: IconComponent,
	decorators: [
		moduleMetadata({
			declarations: [IconComponent],
			imports: [
				BrowserModule,
				CommonModule,
				HttpClientModule,
				AngularSvgIconModule.forRoot(),
			],
		}),
	],
};

export const IconBase = () => ({
	component: IconComponent,
	props: {
		search,
		trash,
		settings,
	},
	template: `
		<div style="padding: 16px; max-width: 480px;">
			<ui-icon [source]="trash"></ui-icon>
			<ui-icon [source]="settings"></ui-icon>
			<ui-icon [source]="search"></ui-icon> <br>
			<ui-icon [source]="trash" size="giant"></ui-icon>
			<ui-icon [source]="settings" size="giant"></ui-icon>
			<ui-icon [source]="search" size="giant"></ui-icon>
		</div>
		<div style="padding: 16px; max-width: 480px; background-color: #fff">
			<ui-icon color="dark" [source]="trash"></ui-icon>
			<ui-icon color="dark" [source]="settings"></ui-icon>
			<ui-icon color="dark" [source]="search"></ui-icon> <br>
			<ui-icon color="dark" [source]="trash" size="giant"></ui-icon>
			<ui-icon color="dark" [source]="settings" size="giant"></ui-icon>
			<ui-icon color="dark" [source]="search" size="giant"></ui-icon>
		</div>
		<div style="padding: 16px; max-width: 480px; background-color: #333">
			<ui-icon color="light" [source]="trash"></ui-icon>
			<ui-icon color="light" [source]="settings"></ui-icon>
			<ui-icon color="light" [source]="search"></ui-icon> <br>
			<ui-icon color="light" [source]="trash" size="giant"></ui-icon>
			<ui-icon color="light" [source]="settings" size="giant"></ui-icon>
			<ui-icon color="light" [source]="search" size="giant"></ui-icon>
		</div>
	`,
});

IconBase.story = {
	name: 'examples',
};

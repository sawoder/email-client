import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

type TextMode = 'header' | 'subheader' | 'base' | 'small' | 'label' | 'pseudo-link';
type TextColor = 'primary' | 'secondary' | 'error' | 'dark' | 'light';

@Component({
	selector: 'ui-text',
	templateUrl: 'text.component.html',
	styleUrls: ['text.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextComponent implements OnInit {
	@Input() mode: TextMode = 'base';
	@Input() color: TextColor = 'primary';
	@Input() bold?: boolean | string;
	@Input() nowrap?: boolean | string;
	@Input() overflowEllipsis?: boolean | string;
	@Input() block?: boolean | string;
	@Input() inline?: boolean | string;

	@Input() labelFor?: string;

	constructor() {}

	classes(): Record<string, boolean> {
		return {
			header: this.mode === 'header',
			subheader: this.mode === 'subheader',
			base: this.mode === 'base',
			small: this.mode === 'small',
			label: this.mode === 'label',
			'pseudo-link': this.mode === 'pseudo-link',

			primary: this.color === 'primary',
			secondary: this.color === 'secondary',
			error: this.color === 'error',
			dark: this.color === 'dark',
			light: this.color === 'light',

			bold: Boolean(this.bold || this.bold === ''),
			nowrap: Boolean(this.nowrap || this.nowrap === ''),
			overflowEllipsis: Boolean(this.overflowEllipsis || this.overflowEllipsis === ''),
			block: Boolean(this.block || this.block === ''),
			inline: Boolean(this.inline || this.inline === ''),
		};
	}

	ngOnInit(): void {}
}

import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

import arrowBack from '!!raw-loader!material-design-icons/navigation/svg/production/ic_arrow_back_24px.svg';
import arrowForward from '!!raw-loader!material-design-icons/navigation/svg/production/ic_arrow_forward_24px.svg';
import moreHoriz from '!!raw-loader!material-design-icons/navigation/svg/production/ic_more_horiz_24px.svg';

type ConstructUrl = (page: number) => string;

@Component({
	selector: 'ui-pagination',
	templateUrl: 'pagination.component.html',
	styleUrls: ['pagination.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaginationComponent implements OnInit {
	@Input() allPages = 0;
	@Input() selectedPage = 0;
	arrowBackIcon = arrowBack;
	arrowForwardIcon = arrowForward;
	moreHorizIcon = moreHoriz;

	constructor() {}

	get hasPrevious(): boolean {
		return this.selectedPage > 1;
	}

	get hasNext(): boolean {
		return this.selectedPage < this.allPages;
	}

	get buttons(): number[] {
		const buttons: number[] = [this.selectedPage];
		if (this.selectedPage > 1) {
			buttons.unshift(this.selectedPage - 1);
		}

		if (this.selectedPage > 3) {
			buttons.unshift(-1);
		}

		if (this.selectedPage > 2) {
			buttons.unshift(1);
		}

		if (this.allPages - this.selectedPage >= 1) {
			buttons.push(this.selectedPage + 1);
		}

		if (this.allPages - this.selectedPage >= 3) {
			buttons.push(-1);
		}

		if (this.allPages - this.selectedPage >= 2) {
			buttons.push(this.allPages);
		}

		return buttons;
	}

	@Input() urlBuilder: ConstructUrl = pageNumber => `/${pageNumber}`;

	ngOnInit(): void {}

	trackByButtons(index: number, button: number): number {
		return button;
	}
}

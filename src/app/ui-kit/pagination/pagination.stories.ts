import { PaginationComponent } from './pagination.component';
import { moduleMetadata } from '@storybook/angular';
import { ButtonComponent } from '../button/button.component';
import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { IconComponent } from '../icon/icon.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { TextComponent } from '../text/text.component';
import { RouterModule } from '@angular/router';

export default {
	title: 'Pagination',
	component: PaginationComponent,
	decorators: [
		moduleMetadata({
			declarations: [TextComponent, PaginationComponent, ButtonComponent, IconComponent],
			imports: [
				BrowserModule,
				CommonModule,
				HttpClientModule,
				RouterModule.forRoot([{ path: '**', component: TextComponent }]),
				AngularSvgIconModule.forRoot(),
			],
			providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
		}),
	],
};

export const PaginationBase = () => ({
	component: PaginationComponent,
	props: {},
	template: `
		<div style="margin: 24px; max-width: 400px;">
			<ui-pagination [allPages]="1" [selectedPage]="1"></ui-pagination> <hr>
			<ui-pagination [allPages]="2" [selectedPage]="1"></ui-pagination> <hr>
			<ui-pagination [allPages]="3" [selectedPage]="1"></ui-pagination> <hr>
			<ui-pagination [allPages]="4" [selectedPage]="1"></ui-pagination> <hr>
			<ui-pagination [allPages]="3" [selectedPage]="3"></ui-pagination> <hr>
			<ui-pagination [allPages]="20" [selectedPage]="15"></ui-pagination> <hr>
		</div>
	`,
});

PaginationBase.story = {
	name: 'examples',
};

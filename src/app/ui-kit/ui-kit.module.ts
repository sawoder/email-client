import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AngularSvgIconModule } from 'angular-svg-icon';

import { AvatarComponent } from './avatar/avatar.component';
import { ButtonComponent } from './button/button.component';
import { HeadlineComponent } from './headline/headline.component';
import { HighlightComponent } from './highlight/highlight.component';
import { IconComponent } from './icon/icon.component';
import { LogoComponent } from './logo/logo.component';
import { PaginationComponent } from './pagination/pagination.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { TextComponent } from './text/text.component';
import { ToggleComponent } from './toggle/toggle.component';

@NgModule({
	exports: [
		LogoComponent,
		TextComponent,
		IconComponent,
		ButtonComponent,
		HeadlineComponent,
		AvatarComponent,
		PaginationComponent,
		ToggleComponent,
		HighlightComponent,
		SpinnerComponent,
	],
	declarations: [
		LogoComponent,
		TextComponent,
		IconComponent,
		ButtonComponent,
		HeadlineComponent,
		AvatarComponent,
		PaginationComponent,
		ToggleComponent,
		HighlightComponent,
		SpinnerComponent,
	],
	imports: [RouterModule, CommonModule, AngularSvgIconModule, ReactiveFormsModule],
})
export class UiKitModule {}

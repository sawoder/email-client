declare module '*.svg' {
	const svg: string;
	export default svg;
}

declare module 'deepcopy' {
	const copy: <T>(argument: T) => T;
	export default copy;
}
